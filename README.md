# hCard Builder

## About this project

Interface for an hCard builder.
- Valid HTML and best practices.  Tested with Lighthouse and HTML_Codesniffer by Squiz
- Microformat - The preview uses hCard conventions outlined in [microformats.org](http://microformats.org/wiki/h-card) with backward compatibility to v-card.
- Validation - The form has validation hooked up to it so all that all the fields are required, as well as email validation for the email field.
- Accessible - Extra care have been put in to make sure this project is keyboard accessible, screen reader friendly, and colour blind friendly.

## Responsive

This project was built with **mobile-first** in mind.  Feel free to try it out on your phone. The maximum container width is set at 1440px. Anything above 1440px will just center the container on the screen.

## Browser support

This project has only been tested on Chrome for desktop and mobile. It should however, work for all other modern browsers.

## Things to improve on

### Testing

I spent a lot of time on making the project look nice and work responsively for mobile and desktop. So I didn't have enough time to create any unit testing or end-to-end testing that I was initially planning to do. I am well versed with Jest and have dabbled with Cypress for end to end testing.  This project is perfect for Cypress testing as there is very little logic involved for unit testing.

### Style with a utility library

I deliberately didn't use a utility library for this.  I wanted to showcase my css ability. If I utilized something like TailwindCSS, it would have saved me a lot more time.

### Skip links

There's room to add a skip link to let the user quickly skip over to the preview pane.

## UI Library

I could have used a UI library like Veutify to speed up the process instead of custom building the UI.  But again, this is to showcase what I can do.
## Total time taken

Having worked on this on and off, I used approximately 6 hours to get it to where it is now.

---
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
